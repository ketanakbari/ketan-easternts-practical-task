<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// auth routes
Route::post('/login', 'AuthAPIController@login');
Route::post('/register', 'AuthAPIController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['auth:api']], function () {
    Route::resource('users', 'UserAPIController');
    Route::post('users/{id}/update', 'UserAPIController@update');
    Route::get('profile', 'UserAPIController@getProfile');
    Route::get('logout', 'AuthAPIController@logout');
});

Route::get('users-export', 'UserAPIController@exportUserCSV');
// able to access without authentication
Route::get('countries', 'CountryAPIController@getCountries');
Route::get('countries/{country_name}/states', 'CountryAPIController@getStatesByCountries');
