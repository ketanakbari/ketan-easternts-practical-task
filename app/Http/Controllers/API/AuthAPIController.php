<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ApiOperationFailedException;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Traits\ImageTrait;
use Auth;
use Exception;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AuthAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (empty($email) or empty($password)) {
            return $this->sendError('username and password required', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var User $user */
        $user = User::whereEmail(strtolower($email))->first();
        if (empty($user)) {
            return $this->sendError('Invalid username or password', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!Hash::check($password, $user->password)) {
            return $this->sendError('Invalid username or password', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        return $this->sendResponse(['token' => $tokenResult->accessToken, 'user' => $user], 'Logged in successfully.');
    }

    /**
     * @param CreateUserAPIRequest $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function register(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = User::create($input);

        if (empty($user)) {
            throw new BadRequestHttpException('User not found.');
        }

        $this->updateProfilePhoto($user, $input);

        return $this->sendSuccess('You are successfully registered');
    }

    public function updateProfilePhoto(User $user, $input) {
        try {
            $options = ['height' => User::HEIGHT, 'width' => User::WIDTH];
            if (!empty($input['photo'])) {
                $input['photo_url'] = ImageTrait::makeImage($input['photo'], User::$PATH, $options);

                $oldImageName = $user->photo_url;
                if (!empty($oldImageName)) {
                    $user->deleteImage();
                }
            }

            $user->update($input);
        } catch (Exception $e) {
            throw new ApiOperationFailedException($e->getMessage());
        }
    }

    public function logout()
    {
        $authUser = Auth::user();;
        $userTokens = $authUser->tokens;

        foreach ($userTokens as $token) {
            /** var Laravel\Passport\Token $token */
            $token->revoke();
        }

        return $this->sendSuccess('Logged out successfully.');
    }
}
