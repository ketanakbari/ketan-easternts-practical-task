<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ApiOperationFailedException;
use App\Exports\UsersExport;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Traits\ImageTrait;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Response;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */
class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);

        return $this->sendResponse($user->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }


    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);
        $this->updateProfilePhoto($user, $input);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * @param User $user
     * @param $input
     * @throws ApiOperationFailedException
     */
    public function updateProfilePhoto(User $user, $input) {
        try {
            $options = ['height' => User::HEIGHT, 'width' => User::WIDTH];
            if (!empty($input['photo'])) {
                $input['photo_url'] = ImageTrait::makeImage($input['photo'], User::$PATH, $options);

                $oldImageName = $user->photo_url;
                if (!empty($oldImageName)) {
                    $user->deleteImage();
                }
            }

            $user->update($input);
        } catch (Exception $e) {
            throw new ApiOperationFailedException($e->getMessage());
        }
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendSuccess('User deleted successfully');
    }

    public function getProfile()
    {
        /** @var User $authUser * */
        $authUser = Auth::user();
        $authUser = $authUser->apiObj();
        return $this->sendResponse(['loggedUser' => $authUser], 'Users retrieved successfully.');
    }

    public function exportUserCSV(){
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
