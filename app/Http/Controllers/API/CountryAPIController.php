<?php

namespace App\Http\Controllers\API;

use App\Models\State;
use App\Repositories\CountryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CountryController
 * @package App\Http\Controllers\API
 */
class CountryAPIController extends AppBaseController
{
    /** @var  CountryRepository */
    private $countryRepository;

    public function __construct(CountryRepository $countryRepo)
    {
        $this->countryRepository = $countryRepo;
    }

    /**
     * Display a listing of the Country.
     * GET|HEAD /countries
     *
     * @param Request $request
     * @return Response
     */
    public function getCountries(Request $request)
    {
        $countries = $this->countryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit'),
            ['name']
        );

        return $this->sendResponse($countries->toArray(), 'Countries retrieved successfully');
    }

    /**
     * @param $countryName
     * @return \Illuminate\Http\JsonResponse
     *
     * * @return Response
     */
    public function getStatesByCountries($countryName)
    {
        $states = State::whereCountryName($countryName)->get(['name']);
        return $this->sendResponse($states->toArray(), 'States retrieved successfully');
    }
}
