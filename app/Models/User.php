<?php

namespace App\Models;

use App\Traits\ImageTrait;
use Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package App\Models
 * @version December 10, 2019, 11:45 am UTC
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string password
 * @property string dob
 * @property tinyInteger gender
 * @property string hobbies
 * @property string country
 * @property string state
 * @property string city
 * @property string address_line_1
 * @property string address_line_2
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property \Illuminate\Support\Carbon $dob
 * @property int $gender 0 for Female,1 for Male
 * @property string $hobbies
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $address_line_1
 * @property string $address_line_2
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAddressLine1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAddressLine2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereHobbies($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use SoftDeletes, Notifiable, HasApiTokens, ImageTrait;
    use ImageTrait {
        deleteImage as traitDeleteImage;
    }

    public $table = 'users';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'dob',
        'gender',
        'hobbies',
        'country',
        'state',
        'city',
        'address_line_1',
        'address_line_2',
        'postal_code',
        'photo_url'
    ];

    static $PATH = 'users';
    const HEIGHT = 250;
    const WIDTH = 250;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'dob' => 'date',
        'hobbies' => 'string',
        'country' => 'string',
        'state' => 'string',
        'city' => 'string',
        'address_line_1' => 'string',
        'address_line_2' => 'string',
        'postal_code' => 'string',
        'photo_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'email' => 'required|email|max:255|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
        'password' => 'required',
        'dob' => 'required',
        'gender' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'address_line_1' => 'required'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function apiObj()
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'gender' => $this->gender,
            'dob' => $this->dob,
            'hobbies' => $this->hobbies,
            'country' => $this->country,
            'state' => $this->state,
            'address_line_!' => $this->address_line_1,
            'address_line_2' => $this->address_line_2,
            'postal_code' => $this->postal_code,
        ];
    }

    public function deleteImage()
    {
        $image = $this->getOriginal('photo_url');
        if (empty($image)) {
            return true;
        }

        return $this->traitDeleteImage(self::$PATH . DIRECTORY_SEPARATOR . $image);
    }

    public function getPhotoUrlAttribute($value)
    {
        if (!empty($value)) {
            return $this->imageUrl(self::$PATH . DIRECTORY_SEPARATOR . $value);
        }

        return asset('assets/images/avatar.png');
    }
}
