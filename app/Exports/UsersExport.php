<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $result = [];
        $users = User::all();
        /** @var User $user */
        foreach ($users as $user) {
            $result[] = $user->apiObj();
        }
        return collect($result);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Id',
            'Firstname',
            'Lastname',
            'Email',
            'Gender',
            'DOB',
            'Hobbies',
            'Country',
            'State',
            'Address Line 1',
            'Address Line 1',
            'Postal Code',
        ];
    }
}
