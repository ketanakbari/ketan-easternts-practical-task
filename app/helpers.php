<?php

use App\Models\User;
use Auth;

/**
 * @return int|null
 */
function getLoggedInUserId()
{
    return Auth::id();
}


/**
 * @return User|null
 */
function getLoggedInUser()
{
    return Auth::user();
}
