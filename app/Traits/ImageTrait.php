<?php

namespace App\Traits;

use App\Exceptions\ApiOperationFailedException;
use Exception;
use Intervention\Image\Facades\Image;
use Storage;

/**
 * Trait ImageTrait.
 */
trait ImageTrait
{
    /**
     * @param string $file
     * @return bool
     */
    public static function deleteImage($file)
    {
        if (Storage::exists($file)) {
            Storage::delete($file);

            return true;
        }

        return false;
    }


    /**
     * @param $file
     * @param $path
     *
     * @return string
     * @throws ApiOperationFailedException
     *
     */
    public static function makeImage($file, $path, $options)
    {
        try {
            $fileName = '';
            if (!empty($file)) {
                $extension = $file->getClientOriginalExtension(); // getting image extension
                if (!in_array(strtolower($extension), ['jpg', 'gif', 'png', 'jpeg'])) {
                    throw  new ApiOperationFailedException('invalid image', \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST);
                }
                $originalName = $file->getClientOriginalName();
                $date = \Illuminate\Support\Carbon::now()->format('Y-m-d');
                $originalName = sha1($originalName . time());
                $fileName = $date . '_' . uniqid() . '_' . $originalName . '.' . $extension;
                if (!empty($options)) {
                    $imageThumb = Image::make($file->getRealPath())->fit($options['width'], $options['height']);
                    $imageThumb = $imageThumb->stream();
                    Storage::put($path . DIRECTORY_SEPARATOR . $fileName, $imageThumb->__toString());
                } else {
                    Storage::putFileAs($path, $file, $fileName, 'public');
                }
            }

            return $fileName;
        } catch (Exception $e) {
            \Log::info($e->getMessage());
            throw new ApiOperationFailedException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $path
     * @return mixed
     */
    public function imageUrl($path)
    {
        return $this->urlEncoding(Storage::url($path));
    }

    /**
     * @param $url
     * @return mixed
     */
    function urlEncoding($url)
    {
        $entities = array(
            '%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F',
            '%25', '%23', '%5B', '%5D', '%5C',
        );
        $replacements = array(
            '!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]", "/",
        );

        return str_replace($entities, $replacements, urlencode($url));
    }
}
