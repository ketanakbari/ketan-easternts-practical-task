<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'dob' => $faker->word,
        'gender' => $faker->word,
        'hobbies' => $faker->text,
        'country' => $faker->word,
        'state' => $faker->word,
        'city' => $faker->word,
        'address_line_1' => $faker->text,
        'address_line_2' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
