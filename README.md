## Project setup

- set variables in `.env` and create a virtual host same name as `APP_URL` (see `.env.example`)
- create `db` on phpmyadmin
- composer install
- npm install
- php artisan migrate
- php artisan db:seed  (in the case countries and states not filled, use cmd: php artisan db:seed --class=""CreateCountriesSeeder" )
- php artisan passport:install
- npm run dev
- open virtual host in browser
