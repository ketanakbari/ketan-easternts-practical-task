import axios from 'axios'
import {errorMsgToast} from "../utils/toasts";

const url = process.env.MIX_APP_URL;
const axiosApi = axios.create({
    baseURL: url,
});

axiosApi.interceptors.response.use((res) => {
    return res.data
}, (error) => {
    if (error.response.data.message) {
        errorMsgToast(error.response.data.message)
    }
    return Promise.reject(error)
});

export default axiosApi
