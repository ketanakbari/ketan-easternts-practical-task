import axios from 'axios'
import router from '../router'
import {errorMsgToast} from "../utils/toasts";

const url = process.env.MIX_APP_URL;
const axiosApi = axios.create({
    baseURL: url,
});

axiosApi.interceptors.request.use(config => {
    config.headers = {
        Authorization: `Bearer ${localStorage.getItem('easterntsToken')}`,
        Accept: 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
    };
    return config
});

axiosApi.interceptors.response.use((res) => {
    return res.data
}, (error) => {
    if (error.response.data.message) {
        errorMsgToast(error.response.data.message)
    }
    if (+error.response.status === 401) {
        localStorage.removeItem('easterntsToken');
        router.push('/login')
    }
    return Promise.reject(error)
});

export default axiosApi
