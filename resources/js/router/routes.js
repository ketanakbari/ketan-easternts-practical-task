// layout
const DefaultLayout = () => import('../layouts/DefaultLayout');
// auth
const Login = () => import('../views/auth/Login');
const Register = () => import('../views/auth/Register');
// users
const Users = () => import('../views/users');
const CreateUser = () => import('../views/users/Create');
const EditUser = () => import('../views/users/Edit');
// page not found
import NotFound404 from "../components/NotFound404";

const routes = [
    {
        path: '/login',
        name: 'Login',
        component: Login,
        beforeEnter(to, from, next) {
            if (!localStorage.getItem('easterntsToken')) {
                next()
            } else {
                next('/users')
            }
        },
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        beforeEnter(to, from, next) {
            if (!localStorage.getItem('easterntsToken')) {
                next()
            } else {
                next('/users')
            }
        },
    },
    {
        path: '',
        redirect: '/users',
        name: 'App',
        component: DefaultLayout,
        beforeEnter(to, from, next) {
            if (localStorage.getItem('easterntsToken')) {
                next()
            } else {
                next('/login')
            }
        },
        children: [
            {
                path: 'users',
                name: 'Users',
                component: Users,
            },
            {
                path: '/users/create',
                name: 'Users',
                component: CreateUser,
            },
            {
                path: '/users/:id/edit',
                name: 'Users',
                component: EditUser,
            },
        ],
    },
    {
        path: '**',
        component: NotFound404,
    },
];

export default routes
