// getters
export const COUNTRIES = 'app/COUNTRIES';
export const STATES_BY_COUNTRY = 'app/STATES_BY_COUNTRY';
export const USERS = 'app/USERS';
export const LOGGED_USER = 'app/LOGGED_USER';
//actions
export const APP_LOGIN = 'app/APP_LOGIN';
export const APP_REGISTER = 'app/APP_REGISTER';
export const APP_LOGOUT = 'app/APP_LOGOUT';
export const GET_COUNTRIES = 'app/GET_COUNTRIES';
export const GET_STATES_BY_COUNTRY = 'app/GET_STATE_BY_COUNTRY';
export const GET_USERS = 'app/GET_USERS';
export const GET_LOGGED_USER = 'app/GET_LOGGED_USER';
export const DELETE_USER = 'app/DELETE_USER';
export const GET_USER_BY_ID = 'app/GET_USER_BY_ID';
export const UPDATE_USER = 'app/UPDATE_USER';
//mutations
export const MUTATE_APP_LOGIN = 'app/MUTATE_APP_LOGIN';
export const MUTATE_APP_REGISTER = 'app/MUTATE_APP_REGISTER';
export const MUTATE_APP_LOGOUT = 'app/MUTATE_APP_LOGOUT';
export const MUTATE_GET_COUNTRIES = 'app/MUTATE_GET_COUNTRIES';
export const MUTATE_GET_STATES_BY_COUNTRY = 'app/MUTATE_GET_STATE_BY_COUNTRY';
export const MUTATE_GET_USERS = 'app/MUTATE_GET_USERS';
export const MUTATE_GET_LOGGED_USER = 'app/MUTATE_GET_LOGGED_USER';
export const MUTATE_DELETE_USER = 'app/MUTATE_DELETE_USER';
export const MUTATE_UPDATE_USER = 'app/MUTATE_UPDATE_USER';
