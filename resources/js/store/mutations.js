import * as types from './types'

export default {

    [types.MUTATE_APP_LOGIN]: (state, result) => {
        localStorage.setItem('easterntsToken', result.token);
    },

    [types.MUTATE_GET_COUNTRIES]: (state, countries) => {
        const countriesArr = [];
        countries.forEach((c) => {
            countriesArr.push(c.name);
        });
        state.countries = countriesArr
    },

    [types.MUTATE_GET_STATES_BY_COUNTRY]: (state, states) => {
        const statesArr = [];
        states.forEach((s) => {
            statesArr.push(s.name);
        });
        state.statesByCountry = statesArr
    },

    [types.MUTATE_GET_USERS]: (state, users) => {
        state.users = users
    },

    [types.MUTATE_GET_LOGGED_USER]: (state, res) => {
        state.loggedUser = res.loggedUser
    },

    [types.MUTATE_APP_LOGOUT]: () => {
        localStorage.removeItem('easterntsToken')
    },

    [types.MUTATE_DELETE_USER]: (state, id) => {
        const index = state.users.findIndex(u => +u.id === +id);
        if (index >= 0) {
            state.users.splice(index, 1)
        }
        state.users = state.users.slice()
    },

    [types.MUTATE_UPDATE_USER]: (state, user) => {
        const index = state.users.findIndex(u => +u.id === +user.id);
        if (index >= 0) {
            state.users.splice(index, 1, user)
        }
    },
}
