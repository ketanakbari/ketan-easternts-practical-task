import * as types from './types'

export default {
    [types.COUNTRIES]: (state) => {
        return state.countries
    },

    [types.STATES_BY_COUNTRY]: (state) => {
        return state.statesByCountry
    },

    [types.USERS]: (state) => {
        return state.users
    },

    [types.LOGGED_USER]: (state) => {
        return state.loggedUser
    },
}
