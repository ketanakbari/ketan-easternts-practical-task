import * as types from './types'
import axios from '../api/axios-with-token'
import axiosWithoutToken from '../api/axios-without-token'
import axiosWithFormData from '../api/axios-with-form-data'

export default {
    [types.APP_LOGIN]: async ({commit}, params) => {
        await axiosWithoutToken.post('login', params).then((response) => {
            commit(types.MUTATE_APP_LOGIN, response.data);
            return true
        })
    },

    [types.APP_REGISTER]: async ({commit}, params) => {
        await axiosWithoutToken.post('register', params).then((response) => {
            // commit(types.MUTATE_APP_REGISTER, response.data);
            return true
        })
    },

    [types.GET_COUNTRIES]: async ({commit}) => {
        await axios.get('countries').then((response) => {
            commit(types.MUTATE_GET_COUNTRIES, response.data);
            return true
        })
    },

    [types.GET_STATES_BY_COUNTRY]: async ({commit}, countryName) => {
        await axios.get(`countries/${countryName}/states`).then((response) => {
            commit(types.MUTATE_GET_STATES_BY_COUNTRY, response.data);
            return true
        })
    },

    [types.GET_LOGGED_USER]: async ({commit}) => {
        await axios.get('profile').then((response) => {
            commit(types.MUTATE_GET_LOGGED_USER, response.data);
            return true
        })
    },

    [types.GET_USERS]: async ({commit}) => {
        return axios.get('users').then((response) => {
            commit(types.MUTATE_GET_USERS, response.data);
            return response.data
        })
    },

    [types.APP_LOGOUT]: async ({commit}) => {
        await axios.get('logout').then(() => {
            commit(types.MUTATE_APP_LOGOUT);
            return true
        })
    },

    [types.DELETE_USER]: async ({commit}, id) => {
        return axios.delete(`users/${id}`).then(() => {
            commit(types.MUTATE_DELETE_USER, id);
            return id
        })
    },

    [types.GET_USER_BY_ID]: async ({commit}, id) => {
        return axios.get(`users/${id}`).then((response) => {
            return response.data
        })
    },

    [types.UPDATE_USER]: async ({commit}, user) => {
        return axios.post(`users/${user.get('id')}/update`, user).then((response) => {
            commit(types.MUTATE_UPDATE_USER, response.data);
            return response.data
        })
    },
}
