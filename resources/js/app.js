import Vue from 'vue'
import App from './views/App'
import router from './router'
import {store} from './store'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate, { Validator } from 'vee-validate'
import { VALIDATION_DICTIONARY } from './utils/validation-dictionary'
import en from 'vee-validate/dist/locale/en'
import VueSweetalert2 from 'vue-sweetalert2';
import Toasted from 'vue-toasted';
import './assets/scss/style.scss'

Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(Toasted);
Validator.localize({ en });
Validator.localize(VALIDATION_DICTIONARY);
Vue.use(VeeValidate);

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});

export default app
