export const VALIDATION_DICTIONARY = {
    en: {
        attributes: {
            first_name: 'firstname',
            last_name: 'lastname',
            email: 'email',
            password: 'password',
            repeat_password: 'confirm password',
            postal_code: 'postal code'
        },
    },
};
