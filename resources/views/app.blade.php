<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Ketan Easternts Practical Task</title>
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<div id="app"></div>
<script src="{!!  asset('assets/js/jquery.min.js')!!}"></script>
<script src="{!! asset('assets/js/jquery.dataTables.min.js')!!}"></script>
<script src="{!! asset('assets/js/app.js')!!}"></script>
</body>
</html>
